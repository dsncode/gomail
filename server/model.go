package server

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/smtp"
	"strings"
)

// Mail concept
type Mail struct {
	SenderID string
	ToIds    []string
	Subject  string
	Body     string
}

// SMTPServer concept
type SMTPServer struct {
	host     string
	port     string
	sender   string
	password string
	tls      *tls.Config
	auth     smtp.Auth
	client   *smtp.Client
}

// Connect stablishes a connection to the server
func (s *SMTPServer) Connect() error {
	conn, err := tls.Dial("tcp", s.ServerName(), s.tls)
	if err != nil {
		log.Println(err)
		return err
	}

	client, err := smtp.NewClient(conn, s.host)
	if err != nil {
		log.Println(err)
		return err
	}

	s.client = client
	// step 1: Use Auth
	if err = client.Auth(s.auth); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

// SendEmail send an email
func (s *SMTPServer) SendEmail(mail Mail) error {
	// step 2: add all from and to
	if err := s.client.Mail(mail.SenderID); err != nil {
		log.Println(err)
		return err
	}
	for _, k := range mail.ToIds {
		if err := s.client.Rcpt(k); err != nil {
			log.Println(err)
			return err
		}
	}

	// Data
	w, err := s.client.Data()
	if err != nil {
		log.Println(err)
		return err
	}
	messageBody := mail.BuildMessage()
	_, err = w.Write([]byte(messageBody))
	if err != nil {
		log.Println(err)
		return err
	}

	err = w.Close()
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}

// Close connection to smtp server
func (s *SMTPServer) Close() {
	s.client.Quit()
}

// NewSMTPServerConnection a new mail connection
func NewSMTPServerConnection(host string, port string, senderID string, password string) *SMTPServer {

	auth := smtp.PlainAuth("", senderID, password, host)

	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         host,
	}

	smtpServer := &SMTPServer{
		host: host,
		port: port,
		auth: auth,
		tls:  tlsconfig,
	}

	return smtpServer
}

// ServerName returns server name
func (s *SMTPServer) ServerName() string {
	return s.host + ":" + s.port
}

// BuildMessage builds a msg
func (mail *Mail) BuildMessage() string {
	message := ""
	message += fmt.Sprintf("From: %s\r\n", mail.SenderID)
	if len(mail.ToIds) > 0 {
		message += fmt.Sprintf("To: %s\r\n", strings.Join(mail.ToIds, ";"))
	}

	message += fmt.Sprintf("Subject: %s\r\n", mail.Subject)
	message += "\r\n" + mail.Body

	return message
}
