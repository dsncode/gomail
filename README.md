# GoMail v1.0.0
gomail is a very basic utility that allows to easilly send emails using go language.


## How to use it

it is very simple. for this, first you need to create a connection to a smtp server

```go
server := server.NewSMTPServerConnection("smtp.zoho.com", "465", "$YOUR_EMAIL_ADDRESS", "$YOUR_PASSWORD")
err := server.Connect()
if err != nil {
	panic(err)
}
```

then just create an email

```go
mail := server.Mail{
SenderID: "danny@dsncode.com",
ToIds:    []string{"your_email@gmail.com"},
Subject:  "Email from GOMAIL",
Body:     "Hello world :)",
}
```

and send it

```go
err = server.SendEmail(mail)
if err != nil {
	panic(err)
}
```

finally, do not forget to close your session

```go
server.Close()
```